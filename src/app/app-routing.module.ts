import { Routes, RouterModule } from '@angular/router';
import { CrearEmpleadoComponent } from './pages/crear-empleado/crear-empleado.component';
import { DetallesEmpleadoComponent } from './pages/detalles-empleado/detalles-empleado.component';
import { EditarEmpleadoComponent } from './pages/editar-empleado/editar-empleado.component';
import { EmpleadoComponent } from './pages/empleado/empleado.component';

const routes: Routes = [
    {path: 'empleados', component: EmpleadoComponent},
    {path: 'crearEmpleado', component: CrearEmpleadoComponent},
    {path: 'editarEmpleado/:id', component: EditarEmpleadoComponent},
    {path: 'detallesEmpleado/:id', component: DetallesEmpleadoComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'empleados'},
];

export const AppRoutingModule = RouterModule.forRoot(routes);