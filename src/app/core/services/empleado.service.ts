import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ResponseEmpleado } from '../models/Response/ResponseEmpleado';
import { endpoint } from '../infraestructure/endpoint/endpoint';
import { RequestEmpleado } from '../models/Request/RequestEmpleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) { }

  getEmpleados(): Observable<ResponseEmpleado[]> {
    return this.http.get(endpoint.Empleado).pipe(
      map(response => response as ResponseEmpleado[]),
    );
  }
  create(empleado: RequestEmpleado): Observable<RequestEmpleado> {
    return this.http.post<RequestEmpleado>(endpoint.Empleado, empleado, { headers: this.httpHeaders });
  }
  obtenerId(id: number): Observable<ResponseEmpleado> {
    return this.http.get<ResponseEmpleado>(`${endpoint.Empleado}/${id}`);
  }
  editar(id: number, empleado: RequestEmpleado): Observable<RequestEmpleado> {
    return this.http.put<RequestEmpleado>(`${endpoint.Empleado}/${id}`, empleado, { headers: this.httpHeaders });
  }
  eliminar(id: number): Observable<RequestEmpleado> {
    return this.http.delete<RequestEmpleado>(`${endpoint.Empleado}/${id}`);
  }
}
