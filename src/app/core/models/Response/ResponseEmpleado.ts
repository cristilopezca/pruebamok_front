import { Sexo } from "../enum/sexo";

export class ResponseEmpleado{
    id: number;
    nombre: string;
    apellido: string;
    edad: Date;
    sexo: Sexo;
    correo: string;
}