import { Sexo } from "../enum/sexo";

export class RequestEmpleado{
    nombre: string;
    apellido: string;
    sexo: Sexo;
    correo: string;
}