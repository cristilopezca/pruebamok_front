import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseEmpleado } from 'src/app/core/models/Response/ResponseEmpleado';
import { EmpleadoService } from 'src/app/core/services/empleado.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-detalles-empleado',
  templateUrl: './detalles-empleado.component.html',
  styleUrls: ['./detalles-empleado.component.css']
})
export class DetallesEmpleadoComponent implements OnInit {

  empleado: ResponseEmpleado;
  constructor(private activatedRoute:ActivatedRoute, private empleadoSvc: EmpleadoService, private router: Router) { }

  ngOnInit() {
    this.getId();
  }

  getId() {
    this.activatedRoute.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.empleadoSvc.obtenerId(id).subscribe(empleado => {
          this.empleado = empleado;
        })
      }
    })
  }
  editar(item) {
    this.router.navigate(['/editarEmpleado', item]);
  }
  eliminar(item) {
    Swal.fire({
      title: '¿Esta seguro de eliminar el empleado?',
      text: 'Esta operación es irreversible',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.empleadoSvc.eliminar(item).subscribe(() => {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Empleado eliminado con exito',
            showConfirmButton: false,
            timer: 1500
          });
          this.router.navigate(['empleados']);
        });
      }
    });
  }
  volver(){
    this.router.navigate(['empleados'])
  }
}
