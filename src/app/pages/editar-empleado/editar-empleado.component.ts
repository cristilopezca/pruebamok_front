import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Sexo } from 'src/app/core/models/enum/sexo';
import { ResponseEmpleado } from 'src/app/core/models/Response/ResponseEmpleado';
import { EmpleadoService } from 'src/app/core/services/empleado.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-editar-empleado',
  templateUrl: './editar-empleado.component.html',
  styleUrls: ['./editar-empleado.component.css']
})
export class EditarEmpleadoComponent implements OnInit {

  empleado: ResponseEmpleado;
  empleadoForm: FormGroup;
  sexo: Sexo;
  enumValues: Array<string> = [];

  constructor(private activatedRoute: ActivatedRoute, private empleadoSvc: EmpleadoService, private router: Router, private formBuilder: FormBuilder) {
    this.empleadoForm = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
      sexo: ['', [Validators.required]],
      correo: ['', [Validators.required]]
    })
  }

  ngOnInit() {
    this.getId();
    for (let value in Sexo) {
      if (typeof Sexo[value] === 'number') {
        this.enumValues.push(value);
      }
    }
  }

  getId() {
    this.activatedRoute.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.empleadoSvc.obtenerId(id).subscribe(empleado => {
          this.empleado = empleado;
          console.log("este es id: ", empleado);
          console.log("este es id: ", this.empleado);
        })
      }
    })
  }
  editar(id, empleado) {
    this.empleadoSvc.editar(id, empleado).subscribe(() => {
      this.router.navigate(['empleados']);
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Empleado guardado con exito',
        showConfirmButton: false,
        timer: 1500
      });
    },
      (err) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: err.error.detailMessage,
        });
      });
  }
  volver() {
    this.router.navigate(['empleados'])
  }
}
