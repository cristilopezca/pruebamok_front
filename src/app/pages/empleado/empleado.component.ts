import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResponseEmpleado } from 'src/app/core/models/Response/ResponseEmpleado';
import { EmpleadoService } from 'src/app/core/services/empleado.service';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  empleado: ResponseEmpleado[];
  fileName = 'ExcelSheet.xlsx';
  constructor(private empleadoSvc: EmpleadoService,
    private router: Router) {
  }

  ngOnInit() {
    this.getEmpleadoList();
  }

  getEmpleadoList(fill?) {
    console.log(fill);
    this.empleadoSvc.getEmpleados().subscribe(empleado => {
      this.empleado = empleado;
      if (fill == 2) {
        console.log("llega al if");
        this.porSexo();
      } if (fill == 3) {
        console.log("llega al else");
        this.porEdad();
      }
    });
  }
  agregar() {
    this.router.navigate(['/crearEmpleado']);
  }
  editar(item) {
    this.router.navigate(['/editarEmpleado', item]);
  }
  detalles(item) {
    this.router.navigate(['/detallesEmpleado', item]);
  }
  eliminar(item) {
    Swal.fire({
      title: '¿Esta seguro de eliminar el empleado?',
      text: 'Esta operación es irreversible',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.empleadoSvc.eliminar(item).subscribe(() => {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Empleado eliminado con exito',
            showConfirmButton: false,
            timer: 1500
          });
          this.getEmpleadoList();
        });
      }
    });
  }
  exportexcel(): void {
    /* pass here the table id */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }
  porSexo() {
    this.empleado.sort((a, b) => {
      if (a.sexo < b.sexo) {
        return 1;
      }
      if (a.sexo > b.sexo) {
        return - 1;
      } else {
        return 0;
      }
    })
  }
  porEdad() {
    this.empleado.sort((a, b) => {
      if (a.edad < b.edad) {
        return 1;
      }
      if (a.edad > b.edad) {
        return -1;
      } else {
        return 0;
      }
    })
  }
}
