import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Sexo } from 'src/app/core/models/enum/sexo';
import { ResponseEmpleado } from 'src/app/core/models/Response/ResponseEmpleado';
import { EmpleadoService } from 'src/app/core/services/empleado.service';
import Swal from 'sweetalert2';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-crear-empleado',
  templateUrl: './crear-empleado.component.html',
  styleUrls: ['./crear-empleado.component.css']
})
export class CrearEmpleadoComponent implements OnInit {

  empleadoForm: FormGroup;
  sexo: Sexo;
  enumValues: Array<string> = [];
  empleado: ResponseEmpleado[];
  model: NgbDateStruct;
  date: { year: number, month: number };

  constructor(private formBuilder: FormBuilder, private router: Router, private empleadoSvc: EmpleadoService, private calendar: NgbCalendar) {
    this.empleadoForm = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
      sexo: ['', [Validators.required]],
      fechaNacimiento: ['', [Validators.required]],
      correo: ['', [Validators.required]]
    })
  }

  ngOnInit() {
    for (let value in Sexo) {
      if (typeof Sexo[value] === 'number') {
        this.enumValues.push(value);
      }
    }
  }
  getEmpleadoList() {
    this.empleadoSvc.getEmpleados().subscribe(empleado => {
      this.empleado = empleado;
      console.log("se esta ejecutando");
    });
  }
  regresar() {
    this.router.navigate(['/empleados'])
  }
  create(empleado) {
    let day = empleado.fechaNacimiento.day;
    let month = empleado.fechaNacimiento.month;
    if (empleado.fechaNacimiento.month < 10) {
      month = `0${empleado.fechaNacimiento.month}`;
    }
    if (empleado.fechaNacimiento.day < 10) {
      day = `0${empleado.fechaNacimiento.day}`;
    }
    const date = `${empleado.fechaNacimiento.year}-${month}-${day}`;
    empleado.fechaNacimiento = date;
    this.empleadoSvc.create(empleado).subscribe(() => {
      this.router.navigate(['/empleados'])
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Empleado guardado con exito',
        showConfirmButton: false,
        timer: 1500
      });
    },
      (err) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: err.error.detailMessage,
        });
      });
  }
  selectToday() {
    this.model = this.calendar.getToday();
  }
}
